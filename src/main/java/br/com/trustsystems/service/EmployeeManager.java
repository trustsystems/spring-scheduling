package br.com.trustsystems.service;

import br.com.trustsystems.entities.EmployeeEntity;

import java.util.List;

public interface EmployeeManager
{
	public void addEmployee(EmployeeEntity employee);
    public List<EmployeeEntity> getAllEmployees();
    public void deleteEmployee(Integer employeeId);
}
