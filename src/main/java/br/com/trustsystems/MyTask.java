package br.com.trustsystems;

import br.com.trustsystems.entities.EmployeeEntity;
import br.com.trustsystems.service.EmployeeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class MyTask
{
    private static final Logger logger = LoggerFactory.getLogger(MyTask.class);
    @Autowired
    private EmployeeManager employeeManager;

    public void sayHello()
    {
        logger.info("Task has been invoked");
        EmployeeEntity employee = new EmployeeEntity();

        employee.setEmail("wenderson@trustsystems.com.br");
        employee.setFirstname("Wenderson");
        employee.setLastname("Ferreira de Souza");
        employee.setTelephone("+556196275337");

        employeeManager.addEmployee(employee);

    }
}
