package br.com.trustsystems.dao;


import br.com.trustsystems.entities.EmployeeEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDAO  {
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImpl.class);


	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void addEmployee(EmployeeEntity employee) 
	{
		em.persist(employee);
		logger.info("Saving employee to database");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeEntity> getAllEmployees() 
	{
		return em.createQuery("from EmployeeEntity").getResultList();
	}

	@Override
	public void deleteEmployee(Integer employeeId) {
		EmployeeEntity employee = (EmployeeEntity)em.find(EmployeeEntity.class, employeeId);
		
        if (null != employee) 
		{
        	em.remove(employee);
        }
	}
}
