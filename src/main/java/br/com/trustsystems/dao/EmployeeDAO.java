package br.com.trustsystems.dao;

import br.com.trustsystems.entities.EmployeeEntity;

import java.util.List;

public interface EmployeeDAO 
{
    public void addEmployee(EmployeeEntity employee);
    public List<EmployeeEntity> getAllEmployees();
    public void deleteEmployee(Integer employeeId);
}