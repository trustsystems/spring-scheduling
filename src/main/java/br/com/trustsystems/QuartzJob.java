package br.com.trustsystems;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class QuartzJob extends QuartzJobBean{

    private MyTask myTask;

    public void setMyTask(MyTask myTask) {
        this.myTask = myTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        myTask.sayHello();
    }
}
